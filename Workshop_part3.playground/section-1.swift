// Playground - noun: a place where people can play

import UIKit

// MARK: - Generics

// ArrayやDictionaryには、Generic Typeを指定することが可能です
var array = Array<String>()
array.append("string")

// funcにGeneric Typeを指定することも可能です
func swapTwoValues<T>(inout a: T, inout b: T) {
    let temporaryA = a
    a = b
    b = temporaryA
}

var a = "aaa"
var b = "bbb"
swapTwoValues(&a, &b)
a
b

// funcの引数や返り値もGenericsで縛ることが出来ます
// Equatableを指定すると、Equatableプロトコルを実装している型のみが指定できるようになります
// そうすることで(==)が使えるようになります
func findStringIndex<T: Equatable>(array: [T], valueToFind: T) -> Int? {
    for (index, value) in enumerate(array) {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let strings = ["cat", "dog", "llama", "parakeet", "terrapin"]
if let foundIndex = findStringIndex(strings, "llama") {
    println("The index of llama is \(foundIndex)")
}

