// Playground - noun: a place where people can play

import UIKit

// MARK: - Function

// functionは、func名(引数名: 型) -> 返り値の型で定義します
// 返り値を複数返す事も可能で、またそれぞれの変数に名前を付けることも出来ます
// 名前を付けない場合は、0始まりのindexで指定します。
func echoString(val1: String, val2: String) -> (first: String, second: String) {
    return (val1, val2)
}

let result = echoString("aaa", "bbb")
result.first
result.second

// 複数の返り値があるときに、片方しか使いたくない場合は、必要ない方を_で破棄できます
let (_, bb) = echoString("cc", "dd")
bb

// 引数名は、#をつけるとfunc呼び出し時にラベル名を必要とすることが出来ます
// また、引数名: 型の後ろに、デフォルト値を定義することも出来ます
func greeting(#name: String, greet: String = "Hello") -> String {
    return "\(greet) \(name)."
}

let greet = greeting(name: "Mike")
let night = greeting(name: "Mike", greet: "Good evening")

// funcのネストも可能です
func returnFifteen() -> Int {
    var y = 10
    func add() {
        y += 5
    }
    add()
    return y
}

returnFifteen()

// また、funcの返り値としてfuncを返すことも可能です
func makeIncrementer() -> (Int -> Int) {
    func addOne(number: Int) -> Int {
        return 1 + number
    }
    
    return addOne
}

var increment = makeIncrementer()
increment(9)

// MARK: - Optional
class Residence {
    var numberOfRooms = 1
}

class Person {
    // ?を付けると、変数の型にnilが許容されたことになります。
    var residence: Residence?
}

let john = Person()

if let roomCount = john.residence?.numberOfRooms {
    println("John's residence has \(roomCount) room(s).")
} else {
    println("Unable to retrieve the number of rooms.")
}

// 初期化時に初期値を通して欲しい場合は、initで指定します
class NeoPerson {
    let residence: Residence
    init(residence: Residence) {
        self.residence = residence
    }
}

let neoJohn = NeoPerson(residence: Residence())

// MARK: - Extension

// extensionで、既存タイプにfuncを追加することが出来ます
extension Int {
    func repetitions(task: () -> ()) {
        for i in 0..<self {
            task()
        }
    }
}

5.repetitions({println("Hello!")})

// 更に、型の中にenumや変数も拡張させることもできます
extension Character {
    enum Kind {
        case Vowel, Constant, Other
    }
    
    var kind: Kind {
        switch String(self).lowercaseString {
        case "a", "e", "i", "o", "u":
            return .Vowel
        case "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z":
            return .Constant
        default:
            return .Other
        }
    }
}

if Character("a").kind == .Vowel {
    println("a is Vowel")
}

