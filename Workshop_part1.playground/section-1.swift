// Playground - noun: a place where people can play

import UIKit

// MARK: - Variables & Constants

// 変数の書き方
// var = 変数
// let = 定数
var varStr = "Hello, playground"
let letStr = "Hello, playground"

// 型推論があるため、変数の型を指定する必要はありません
// 型を指定することもできます
let implicitInteger = 70
let implicitDouble = 70.0
let explicitDouble: Double = 70

var explicitInteger: Int = 70
//explicitInteger = 70.0

// 変数には、複数の値を格納することができます
// それぞれの変数にラベルを付ければ、ラベルで値が参照できます
let xyz = (x: 10.0, y: 12.0, z: 13.0)
xyz.x
xyz.y

// 型の変換は、StringからIntの場合はtoInt()を使います
// IntをStringにキャストする場合は、String(var)を使います
var sumOne = 1
var sumTwo = "2"

sumOne + sumTwo.toInt()!
String(sumOne) + sumTwo

// String内に変数を含みたい場合は、\()で変数を囲みます
let apples = 3
let oranges = 3
let appleSummary = "I have \(apples) apples."
let fruitSummary = "I have \(apples + oranges) pieces of fruit."

// MARK: - Array & Dictionary

// Arrayの定義は、直接[]内に値を設定して初期化できます。
// [1]などのIndex指定で値を取得、更新できます
// 追加はappend()を使います
var shoppingList = ["catfish", "water", "tulips", "blue paint"]
shoppingList[1] = "bottle of water"
shoppingList.append("ffff")
shoppingList

// Dictionaryは、key:valueで値を設定して初期化できます。
// [key]といったkey指定で値を取得、更新、追加できます
var occupations = [
    "Malcolm": "Captain",
    "Kaylee":  "Mechanic"
]
occupations["Jayne"] = "Public Relations"
occupations

// MARK: - if & for

// if構文は、カッコを必要としません
if contains(shoppingList, "catfish") {
    println("Shopping list have catfish.")
}

// for-in構文も、if構文と同様です
// また、Dictionaryをfor-inで回した場合は、key:valueのまま取り出せます。
for occupation in occupations {
    println("\(occupation.0) is \(occupation.1)")
}

// for-in構文には、数値の範囲を指定する記法もあります。
// ...か..<が指定でき、0...5だったら0以上5以下、0..<5だったら0以上5未満になります。
for i in 0..<occupations.count {
    let name = Array(occupations.keys)[i]
    let occupation = Array(occupations.values)[i]
    
    println("\(name) is \(occupation)")
}




